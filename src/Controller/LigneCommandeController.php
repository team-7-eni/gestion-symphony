<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Entity\LigneCommande;
use App\Entity\Produit;
use App\Form\LigneCommandeType;
use App\Form\CommandeType;
use App\Repository\LigneCommandeRepository;
use App\Repository\ProduitRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ligne/commande")
 */
class LigneCommandeController extends AbstractController
{
    /**
     * @Route("/", name="ligne_commande_index", methods={"GET", "POST"})
     */
    public function index(Request $request, Request $request2, Request $request3,ProduitRepository $produitRepository, LigneCommandeRepository $ligneCommandeRepository, PaginatorInterface $paginator): Response
    {
        $ligneCommande = new LigneCommande();
        $form = $this->createForm(LigneCommandeType::class, $ligneCommande);
        $form->handleRequest($request);

        $commande = new Commande();
        $formCom = $this->createForm(CommandeType::class, $commande);
        $formCom->handleRequest($request2);

        $ligneCommandes = $paginator->paginate($ligneCommandeRepository->findAllVisibleQuery(), $request3->query->getInt('page', 1), 5);

        if (($form->isSubmitted() && $form->isValid())) {
            $entityManager = $this->getDoctrine()->getManager();

            $produit = $produitRepository->find($ligneCommande->getProduit()->getId());
            $produit->setQte(($produit->getQte() - $ligneCommande->getQteCom()));

            if($produit->getQte() < 0) return $this->redirectToRoute('erreur', [ 'erreur' => "Une erreur est survenue."]);

            $entityManager->persist($commande);
            $entityManager->flush();

            $ligneCommande->setCommande($commande);

            $entityManager->persist($ligneCommande);
            $entityManager->persist($produit);
            $entityManager->flush();


            return $this->redirectToRoute('ligne_commande_index');
        }

        return $this->render('ligne_commande/index.html.twig', [
            'ligne_commandes' => $ligneCommandes,
            'commande' => $commande,
            'form' => $form->createView(),
            'formCom' => $formCom->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ligne_commande_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Request $request2, LigneCommande $ligneCommande): Response
    {
        $form = $this->createForm(LigneCommandeType::class, $ligneCommande);
        $form->handleRequest($request);

        $commande = $ligneCommande->getCommande();
        $formCom = $this->createForm(CommandeType::class, $commande);
        $formCom->handleRequest($request2);

        if ($form->isSubmitted() && $form->isValid()) {

            if($ligneCommande->getProduit()->getQte() < 0 ) return $this->redirectToRoute('erreur', ['erreur' => "une erreur est survenue."]);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ligne_commande_index');
        }


        return $this->render('ligne_commande/edit.html.twig', [
            'ligne_commande' => $ligneCommande,
            'commande' => $commande,
            'form' => $form->createView(),
            'formCom' => $formCom->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ligne_commande_delete")
     */
    public function delete(LigneCommande $ligneCommande): Response
    {
            $ligneCommande->getProduit()->setQte($ligneCommande->getProduit()->getQte() + $ligneCommande->getQteCom());
            $commande = $ligneCommande->getCommande();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ligneCommande);
            $entityManager->remove($commande);
            $entityManager->flush();

        return $this->redirectToRoute('ligne_commande_index');
    }


}
